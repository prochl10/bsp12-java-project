

import java.sql.Array;
import java.util.Vector;

public class NoteList
{
  private Note [] mList;
  private int mCapacity;
  private int mCount;

  //constructor with initial capacity
  public NoteList (int capacity)
  {
    mCount = 0;
    if(capacity<1)
    {
      mCapacity = 1;
      mList = new Note[1];
    }
    else
     {
       mCapacity = capacity;
       mList = new Note[capacity];
     }

     for (int i = 0; i <mCapacity; ++i)
       mList[i]=null;
  }

  //add Note n to the list
  //returns true if successful, otherwise false
  public boolean add (Note n)
  {
    if (mCount>=mCapacity) return false;
    for (int i=0; i< mCapacity; ++i)
    {
      if (mList[i] == null)
      {
        mList[i] = n;
        mCount ++;
        return true;
      }
    }
    return false;
  }

  //returns the first Note object with given info (no deleting!)
  // - otherwise returns null
  public Note contains (String info)
  {
    for (Note n:mList)
    {
      if (n.getInfo().equals(info)) return n;
    }
    return null;
  }

  //returns number of elements in list
  public int getSize ()
  {
    return mCount;
  }

  //check if list is full
  public boolean isFull ()
  {
    return mCount>=mCapacity;
  }

  //returns the whole list as a string
  public String toString ()
  {
    String res = "[";

    for (int i = 0; i <mCapacity; ++i)
    {
      if (mList[i] != null)
        res = res + mList[i].toString();
    }

    res = res + "]";

    return res;
  }

  //deletes the first Note object with given data and returns it
  //returns null if there is no such object in the list
  public Note delete (String info)
  {
    for (int i = 0; i < mCapacity; ++i)
    {
      if(mList[i]!= null && mList[i].getInfo().equals(info))
      {
        Note n = mList[i];
        mList[i] = null;
        mCount--;
        return n;
      }
    }
    return null;
  }

  //returns a NoteList containing all Notes with the give date
  public NoteList getNotesOnDate (Date date)
  {
    Vector<Integer> resIndex = new Vector<>();
    int resCount = 0;
    Date d = null;
    for (int i = 0; i < mCapacity; ++i)
    {
      if (mList[i]!=null)
        d = mList[i].getDate();
      if(mList[i]!= null && d!=null && d.equals(date))
      {
        resIndex.add(i);
        resCount ++;
      }
    }
    NoteList res = new NoteList(resCount);
    for (int i = 0; i < resCount; ++i)
    {
      res.add(mList[resIndex.elementAt(i)]);
    }
    return res;
  }
}
