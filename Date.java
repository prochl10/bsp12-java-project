public class Date
{
  private int mDay;
  private int mMonth;
  private int mYear;


  //constructor
  public Date (int day, int month, int year)
  {
    mDay = day;
    mMonth = month;
    mYear = year;
  }

  public int getDay ()
  {
    return mDay;
  }

  public int getMonth ()
  {
    return mMonth;
  }

  public int getYear ()
  {
    return mYear;
  }

  //returns true if day, month and year match, otherwise false
  public boolean equals (Date other)
  {
    return (mDay==other.getDay() && mMonth == other.getMonth() && mYear == other.getYear());
  }

  //returns a date as day.month.year (for example 1.1.2019)
  public String toString ()
  {
    return mDay + "." + mMonth + "." + mYear;
  }
}
