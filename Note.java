public class Note
{
  private String mInfo;

  //constructor
  public Note (String info)
  {
    mInfo = info;
  }

  //returns internal info
  public String getInfo ()
  {
    return mInfo;
  }

  //returns the internal info as: (info)
  public String toString()
  {
    return "(" + mInfo + ")";
  }
    //returns null 
  public Date getDate()
  {
    return null;
  }
}
