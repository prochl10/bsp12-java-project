//Lucie Prochazkova, 11834393

import javax.management.NotificationEmitter;

public class Bsp12
{

  public static void main(String [] args)
  {
    Note n1 = new Note("Vorbereitung");
    System.out.print(n1);

    Note n2 = new NoteWithDate("Test", new Date (1, 1, 2019));
    System.out.print(n2);


    NoteList myNotes = new NoteList(5);
    System.out.println(myNotes);
    Note n3 = new NoteWithDate ("WS-Ende", new Date (31, 1, 2019));
    myNotes.add(n1);
    myNotes.add(n2);
    myNotes.add(n3);
    System.out.println(myNotes);

    myNotes.add(new NoteWithDate("Semesterbeginn", new Date(1, 3, 2019)));
    myNotes.add(new Note("Ticket kaufen"));
    System.out.println(myNotes);
    Note n = myNotes.delete("Test");
    if (n != null)
      System.out.println("gefunden und entfernt: " + n);
    else
      System.out.println("nicht gefunden");

    Note n4 = new NoteWithDate ("Daten sichern", new Date (1, 3, 2019));
    myNotes.add(n4);
    System.out.println(myNotes);
    System.out.println(myNotes.getNotesOnDate(new Date(1, 3, 2019)));
    System.out.println(myNotes.getNotesOnDate(new Date(31, 12, 2018)));

//    (Plant a tree|16.11.2021)(Drive with a train|11.7.2021)(Turn on the television)(Turn on the television|20.4.2022)
    NoteList notes  = new NoteList(4);
    Note a1 = new NoteWithDate("Plant a tree", new Date(16, 11, 2021));
    Note a2 = new NoteWithDate("Drive with a train", new Date(11, 7, 2021));
    Note a3 = new Note("Turn on the television");
    Note a4 = new NoteWithDate("Turn on the television", new Date(20,4,2022));
    notes.add(a1);
    notes.add(a2);
    notes.add(a3);
    notes.add(a4);
    NoteList dateNotes = notes.getNotesOnDate(new Date(11, 7, 2021));
    System.out.println(dateNotes.getSize());
  }
}
