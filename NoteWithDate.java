public class NoteWithDate extends Note
{
  private Date mDate;
  public NoteWithDate(String info, Date date)
  {
    super(info);
    mDate = date;
  }

  //returns the date
  public Date getDate ()
  {
    return mDate;
  }

  public String toString()
  {
    return "(" + super.getInfo() + "|" + mDate.toString() + ")";
  }


}
